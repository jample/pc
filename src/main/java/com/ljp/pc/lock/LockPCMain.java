package com.ljp.pc.lock;


import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockPCMain {
    /**
     * 运行结果展示，可能结果顺序不一致
     * 队列为空，消费者(consumer-thread1)等待生产者生产
     * 队列为空，消费者(consumer-thread2)等待生产者生产
     * 生产者(producer-thread1)生产，值为：0
     * 生产者(producer-thread2)生产，值为：1
     * 消费者(consumer-thread1)消费，值为：0
     * 消费者(consumer-thread2)消费，值为：1
     * 生产者(producer-thread3)生产，值为：2
     * 生产者(producer-thread4)生产，值为：3
     * 消费者(consumer-thread1)消费，值为：2
     * 消费者(consumer-thread1)消费，值为：3
     * 队列为空，消费者(consumer-thread2)等待生产者生产
     * 生产者(producer-thread4)生产，值为：4
     * 消费者(consumer-thread2)消费，值为：4
     * 队列为空，消费者(consumer-thread1)等待生产者生产
     * 队列为空，消费者(consumer-thread2)等待生产者生产
     * 生产者(producer-thread2)生产，值为：5
     * 消费者(consumer-thread1)消费，值为：5
     * 队列为空，消费者(consumer-thread2)等待生产者生产
     * 生产者(producer-thread2)生产，值为：6
     * 消费者(consumer-thread2)消费，值为：6
     * 生产者(producer-thread1)生产，值为：7
     * 消费者(consumer-thread2)消费，值为：7
     * 生产者(producer-thread3)生产，值为：8
     * 生产者(producer-thread4)生产，值为：9
     */
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        Condition fullCondition = lock.newCondition();
        Condition emptyCondition = lock.newCondition();
        Queue<Integer> queue = new LinkedList<>();
        Producer producer = new Producer(queue, 15, fullCondition, emptyCondition, lock);
        Consumer consumer = new Consumer(queue, 15, fullCondition, emptyCondition, lock);
        Thread producerThread1 = new Thread(producer, "producer-thread1");
        Thread producerThread2 = new Thread(producer, "producer-thread2");
        Thread producerThread3 = new Thread(producer, "producer-thread3");
        Thread producerThread4 = new Thread(producer, "producer-thread4");
        Thread consumerThread1 = new Thread(consumer, "consumer-thread1");
        Thread consumerThread2 = new Thread(consumer, "consumer-thread2");
        consumerThread1.start();
        consumerThread2.start();
        producerThread1.start();
        producerThread2.start();
        producerThread3.start();
        producerThread4.start();
    }
}
