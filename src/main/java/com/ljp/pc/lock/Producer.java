package com.ljp.pc.lock;

import java.util.Queue;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Producer implements Runnable {
    private final Queue<Integer> queue;
    private int maxSize;
    private final Condition fullCondition;
    private final Condition emptyCondition;
    private final Lock lock;
    volatile Integer offerValue = 0;

    public Producer(Queue<Integer> queue, int maxSize, Condition fullCondition, Condition emptyCondition, Lock lock) {
        this.queue = queue;
        this.maxSize = maxSize;
        this.fullCondition = fullCondition;
        this.emptyCondition = emptyCondition;
        this.lock = lock;
    }

    @Override
    public void run() {

        while (true) {
            try {
                lock.lock();

                while (queue.size() >= maxSize) {
                    System.out.println("队列已满，生产者(" + Thread.currentThread().getName() + ")等待消费者消费");
                    try {
                        fullCondition.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("生产者(" + Thread.currentThread().getName() + ")生产，值为：" + offerValue);
                queue.offer(offerValue);
                offerValue++;
                emptyCondition.signalAll();
                fullCondition.signalAll();
            } finally {
                lock.unlock();
            }
            try {
                //随机生产
                Thread.sleep(new Random().nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
