package com.ljp.pc.object;

import java.util.Queue;
import java.util.Random;

public class Consumer implements Runnable {
    private final Queue<Integer> queue;
    private int maxSize;

    public Consumer(Queue<Integer> queue, int maxSize) {
        this.queue = queue;
        this.maxSize = maxSize;
    }


    @Override
    public void run() {
        while (true) {
            synchronized (queue) {
                while (queue.isEmpty()) {
                    try {
                        System.out.println("队列为空，消费者(" + Thread.currentThread().getName() + ")等待生产者生产");
                        queue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                int poll = queue.poll();
                System.out.println("消费者(" + Thread.currentThread().getName() + ")消费，值为：" + poll);
                queue.notifyAll();
                try {
                    //随机消费
                    Thread.sleep(new Random().nextInt(10000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
