package com.ljp.pc.object;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 运行结果展示，可能结果顺序不一致
 * 队列为空，消费者(consumer-thread1)等待生产者生产
 * 生产者(producer-thread1)生产，值为：0
 * 生产者(producer-thread1)生产，值为：1
 * 生产者(producer-thread1)生产，值为：2
 * 生产者(producer-thread1)生产，值为：3
 * 生产者(producer-thread1)生产，值为：4
 * 消费者(consumer-thread1)消费，值为：0
 * 生产者(producer-thread1)生产，值为：5
 * 生产者(producer-thread1)生产，值为：6
 * 生产者(producer-thread1)生产，值为：7
 * 生产者(producer-thread1)生产，值为：8
 * 生产者(producer-thread1)生产，值为：9
 * 生产者(producer-thread1)生产，值为：10
 * 生产者(producer-thread1)生产，值为：11
 * 生产者(producer-thread1)生产，值为：12
 * 生产者(producer-thread1)生产，值为：13
 * 生产者(producer-thread1)生产，值为：14
 * 生产者(producer-thread1)生产，值为：15
 * 队列已满，生产者(producer-thread1)等待消费者消费
 * 消费者(consumer-thread1)消费，值为：1
 * 消费者(consumer-thread1)消费，值为：2
 * 消费者(consumer-thread1)消费，值为：3
 * 消费者(consumer-thread1)消费，值为：4
 * 消费者(consumer-thread1)消费，值为：5
 * 消费者(consumer-thread1)消费，值为：6
 * 生产者(producer-thread1)生产，值为：16
 * 生产者(producer-thread1)生产，值为：17
 * 生产者(producer-thread1)生产，值为：18
 * 生产者(producer-thread1)生产，值为：19
 * 消费者(consumer-thread1)消费，值为：7
 * 消费者(consumer-thread1)消费，值为：8
 */
public class ObjectPCMain {
    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();
        Producer producer = new Producer(queue, 15);
        Consumer consumer = new Consumer(queue, 15);
        Thread producerThread1 = new Thread(producer, "producer-thread1");
        Thread producerThread2 = new Thread(producer, "producer-thread2");
        Thread producerThread3 = new Thread(producer, "producer-thread3");
        Thread producerThread4 = new Thread(producer, "producer-thread4");
        Thread consumerThread1 = new Thread(consumer, "consumer-thread1");
        Thread consumerThread2 = new Thread(consumer, "consumer-thread2");
        consumerThread1.start();
        consumerThread2.start();
        producerThread1.start();
        producerThread2.start();
        producerThread3.start();
        producerThread4.start();
    }
}
