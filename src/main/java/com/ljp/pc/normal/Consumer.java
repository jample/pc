package com.ljp.pc.normal;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {
    private final BlockingQueue<Integer> queue;

    public Consumer(BlockingQueue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                //take会阻塞
                Integer take = queue.take();
                System.out.println("消费者(" + Thread.currentThread().getName() + ")消费：" + take);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
