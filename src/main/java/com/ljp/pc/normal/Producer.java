package com.ljp.pc.normal;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {
    private final BlockingQueue<Integer> queue;

    public Producer(BlockingQueue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("生产者(" + Thread.currentThread().getName() + ")生产：" + i);
            try {
                //put会阻塞
                queue.put(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
