package com.ljp.pc.normal;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class NormalPCMain {
    /**
     * 运行结果展示，可能结果顺序不一致
     * 生产者(producer-thread)生产：0
     * 生产者(producer-thread)生产：1
     * 生产者(producer-thread)生产：2
     * 生产者(producer-thread)生产：3
     * 消费者(consumer-thread1)消费：0
     * 消费者(consumer-thread1)消费：2
     * 消费者(consumer-thread2)消费：1
     * 生产者(producer-thread)生产：4
     * 消费者(consumer-thread1)消费：3
     * 消费者(consumer-thread2)消费：4
     * 生产者(producer-thread)生产：5
     * 生产者(producer-thread)生产：6
     * 消费者(consumer-thread1)消费：5
     * 消费者(consumer-thread2)消费：6
     * 生产者(producer-thread)生产：7
     * 生产者(producer-thread)生产：8
     * 消费者(consumer-thread1)消费：7
     * 消费者(consumer-thread2)消费：8
     * 生产者(producer-thread)生产：9
     * 消费者(consumer-thread1)消费：9
     */
    public static void main(String[] args) {
        /**
         * BlockingQueue是一个阻塞队列，它的存取可以保证只有一个线程在进行，
         * 所以根据逻辑，生产者在内存满的时候进行等待，并且唤醒消费者队列，
         * 反过来消费者在饥饿状态下等待并唤醒生产者进行生产。
         */
        BlockingQueue<Integer> queue = new LinkedBlockingDeque<>();
        Producer producer = new Producer(queue);
        Consumer consumer = new Consumer(queue);
        Thread producerThread1 = new Thread(producer, "producer-thread1");
        Thread consumerThread1 = new Thread(consumer, "consumer-thread1");
        Thread consumerThread2 = new Thread(consumer, "consumer-thread2");
        consumerThread1.start();
        consumerThread2.start();
        producerThread1.start();
    }
}
